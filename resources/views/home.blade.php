<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"/>
  <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
  <title>Home</title>
</head>
<body>
  <div class="profile-card">
    <div class="card-header">
      <div class="pic">
        <img src="{{ asset('/images/pusu2.jpg') }}" alt="">
      </div>
      <div class="name">Abu Rizal</div>
      <div class="desc">Developer & Designer</div>
      <div class="sm">
        <a href="#" class="fab fa-facebook-f"></a>
        <a href="#" class="fab fa-github"></a>
        <a href="#" class="fab fa-instagram"></a>
      </div>
      <div class="button">
        <a href="#" class="contact-btn">Follow</a>
        <a href="{{ url('profile') }}" class="view-btn">View Profile</a>
      </div>
    </div>
    <div class="card-footer">
      <div class="numbers">
        <div class="item">
          <span>10</span>
          Posts
        </div>
        <div class="border"></div>
        <div class="item">
          <span>2</span>
          Projects
        </div>
        <div class="border"></div>
        <div class="item">
          <span>120</span>
          Followers
        </div>
      </div>
    </div>
  </div>
</body>
</html>