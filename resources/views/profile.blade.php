<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"/>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <title>Profile</title>
</head>
<body>
    <div class="container">
        <div class="profile">
            <div class="back">
            <a href="{{ url('/') }}" class="fas fa-arrow-left"></a>
            </div>
            <div class="pic">
                <img src="{{ asset('/images/pusu2.jpg') }}" alt="">
            </div>
            <div class="name">Abu Rizal</div>
            <div class="desc">Developer & Designer</div>
            <div class="sm">
                <a href="#" class="fab fa-facebook-f"></a>
                <a href="#" class="fab fa-github"></a>
                <a href="#" class="fab fa-instagram"></a>
            </div>
            <div class="button">
                <a href="#" class="contact-btn">Follow</a>
            </div>
        </div>
        <div class="details">
            <div class="header">
                <div class="numbers">
                    <div class="item">
                    <span>10</span>
                    Posts
                    </div>
                    <div class="border"></div>
                    <div class="item">
                    <span>2</span>
                    Projects
                    </div>
                    <div class="border"></div>
                    <div class="item">
                    <span>120</span>
                    Followers
                    </div>
                </div>
            </div>
            <div class="bio">
                <h3>Bio:</h3>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto quaerat aut deleniti rem exercitationem repellat sequi, excepturi, asperiores cumque enim ea voluptatum. Commodi sit fugit unde sed nulla. Ullam, ducimus.
            </div>
            <div class="birthday"><i class="fas fa-heart"></i>20-08-2001</div>
            <div class="phone"><i class="fas fa-phone"></i>+63-838-4881-5730</div>
            <div class="e-mail"><i class="fas fa-envelope"></i>daviarpan@gmail.com</div>
            <div class="address"><i class="fas fa-home"></i>Banyuwangi, East Java</div>
        </div>
    </div>
</body>
</html>