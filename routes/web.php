<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeC;
use App\Http\Controllers\ProfileC;

Route::get('/',[HomeC::class,'index']);
Route::get('/profile',[ProfileC::class,'index']);